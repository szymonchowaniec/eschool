<%--
  Created by IntelliJ IDEA.
  User: dawidvanrijswijk
  Date: 2018-11-23
  Time: 21:29
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="py-5 text-center align-items-center d-flex"
     style="background-image: linear-gradient(to left bottom, rgba(189, 195, 199, .75), rgba(44, 62, 80, .75)); background-size: 100%;">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-8 mx-auto"><i class="d-block fa fa-stop-circle mb-3 text-muted fa-5x"></i>
                <h1 class="display-3 mb-4">Szkolenie Rajdowe</h1>
                <p class="lead mb-5"><b>Prezent zawiera:<br></b>• wsparcie profesjonalnego instruktora<br>• krótkie
                    szkolenie<br>• 6 okrążeń za kierownicą<br>• 1 okrążenie zapoznawcze<br>• katalog - poradnik
                    dotyczący technik rajdowej jazdy<br><br><b>Lokalizacja:</b><br>Tor Warszawa - Bemowo<br><br><b>Wstępny
                        harmonogram jazd na sezon 2018:</b>&nbsp;<br>17.03/07.04/06.05/27.05/16.06/29.07/19.08/08.09/29.09/20.10<br><br>Szukasz
                    prezentu dla prawdziwego fana motoryzacji?&nbsp;<br>Z pewnością rajdowe szkolenie będzie wspaniałym
                    pomysłem na prezent dla męża, brata czy ojca.<br>Podaruj wyjątkowy prezent na urodziny lub święta!&nbsp;<br>Takiego
                    prezentu i emocji po prostu nie da się zapomnieć!&nbsp;<br>Istnieje możliwość nagrania przejazdu
                    kamerą Go Pro, warunkiem jest pzyniesienie na realizację własnej karty SD.&nbsp; &nbsp;</p> <a
                        href="#" class="btn btn-lg btn-primary mx-1">Dodaj do kalendarza</a>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>

</html>