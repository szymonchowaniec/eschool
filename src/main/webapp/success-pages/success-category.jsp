<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-24
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="container">
    <br>
    <label>Gratulacje! Dodałeś nową kategorię kursów!</label><br>
    <br>
    <label>Co chcesz teraz zrobić?</label><br>
    <br>
    <div class="py-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="/admin/create/course">
                        <button type="button" class="btn btn-success">Dodaj kurs</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="">
                        <button type="button" class="btn btn-primary">Zarządzaj kategoriami</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="">
                    <button type="button" class="btn btn-primary">Zarządzaj kursami</button>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="/admin">
                    <button type="button" class="btn btn-primary">Wróć do menu</button>
                </a>
            </div>
        </div>
    </div>
</div>


<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>
