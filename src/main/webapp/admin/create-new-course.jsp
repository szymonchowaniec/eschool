<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-27
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-24
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<form:form action="/admin/create/course" modelAttribute="courseAdminDto" method="post">
    <div class="container">
        <br>
        <label>Wybierz kategorię dla tego kursu:</label><form:errors path="name"/><br>

        <div class="form-row">
            <div class="form-group col-md-6">
                <select class="form-control mt-2" name="sportCard">
                    <c:forEach var="category" items="${categories}" varStatus="loop">
                        <option>${category.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="CourseName">Nazwa kursu:</label>
            <p class="mb-3 mt-2"><form:errors path="name" class="alert alert-danger mb-2"/></p>
            <form:input path="name" class="form-control" id="CourseName" placeholder="Wpisz nazwę kursu"/>
        </div>

        <div class="form-group">
            <label for="Description">Krótki opis kursu:</label>
            <p class="mb-3 mt-2"><form:errors path="description" class="alert alert-danger mb-2"/></p>
            <form:textarea path="description" class="form-control" id="Description" rows="3" cols="20"
                           placeholder="Wpisz krótki opis kategorii"/>
        </div>

        <div class="form-group">
            <label for="price">Cena w PLN:</label><br>
            <input class="form-control" id="price" name="price" type="text"
                   placeholder="Wpisz w złotówkach bez nazwy waluty">
        </div>


        <div class="form-group">
            <label for="Image">Zdjęcie kursu:</label><br>
            <input class="form-control" id="Image" name="imagePath" type="text" placeholder="Załaduj obrazek">
        </div>

        <div class="form-group">
            <label for="YouTube">Link do filmu na YouTube:</label><br>
            <input class="form-control" id="YouTube" name="youTubeVideoID" type="text"
                   placeholder="Wklej pełny link do filmu na YouTube">
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Utwórz kurs</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" href="admin/admin-panel.jsp">Wróć</button>
        </div>

    </div>
</form:form>


<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>