<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-24
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>


<div class="container">
    <p><h2>Organizacja grup:</h2></p>
    <div class="row mt-4 mb-4">
        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="/admin/create/category">
                    <button type="button" class="btn btn-success btn-lg btn-block">Utwórz kategorię</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="/admin/create/course">
                    <button type="button" class="btn btn-success btn-lg btn-block">Utwórz kurs</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="/admin/create/group">
                    <button type="button" class="btn btn-success btn-lg btn-block">Utwórz grupę</button>
                </a>
            </div>
        </div>

        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="/admin/manage/category">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Zarządzaj kategoriami</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="/admin/manage/course">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Zarządzaj kursami</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="/admin/manage/group">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Zarządzaj grupami</button>
                </a>
            </div>

        </div>
        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="/admin/create/tutors">
                    <button type="button" class="btn btn-success btn-lg btn-block">Utwórz trenera</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="/admin/manage/tutors">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Zarządzaj trenerami</button>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <p><h2>Użytkownicy:</h2></p>

    <div class="row mt-4 mb-4">
        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-success btn-lg btn-block">Utwórz użytkownika</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-danger btn-lg btn-block">Utwórz administratora</button>
                </a>
            </div>
        </div>

        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-primary btn-lg btn-block">Zarządzaj użytkownikami</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-danger btn-lg btn-block">Zarządzaj administratorami</button>
                </a>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <p><h2>Płatności:</h2></p>

    <div class="row mt-4 mb-4">
        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Podsumowanie dla grup</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Wykresy dla grup</button>
                </a>
            </div>
        </div>

        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-warning btn-lg btn-block">Konfiguracja PayU</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-warning btn-lg btn-block">Komunikacja z PayU</button>
                </a>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <p><h2>Konfiguracja strony:</h2></p>

    <div class="row mt-4 mb-4">
        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Strona główna</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Plan tygodnia</button>
                </a>
            </div>
        </div>

        <div class="col mb-2">
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Rejestracja i logowanie</button>
                </a>
            </div>
            <div class="mt-1 mb-1">
                <a href="#">
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block">Zapisy dla gości</button>
                </a>
            </div>
        </div>
    </div>

</div>


<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>
