<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-27
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<form action="/admin/create/course" , method="post">
    <div class="container">
        <br>
        <div class="form-group">
            <label for="TutorName">Imię:</label><br>
            <input class="form-control" id="TutorName" name="name" type="text" placeholder="Wpisz imię">
        </div>
        <div class="form-group">
            <label for="TutorSurname">Nazwisko:</label><br>
            <input class="form-control" id="TutorSurname" name="name" type="text" placeholder="Wpisz nazwisko:">
        </div>
        <div class="form-group">
            <label for="TutorId">ID nauczyciela:</label><br>
            <input class="form-control" id="TutorId" name="name" type="text" placeholder="Wpisz ID">
        </div>
        <div class="form-group">
            <label for="Image">Zdjęcie nauczyciela:</label><br>
            <input class="form-control" id="Image" name="imagePath" type="text" placeholder="Załaduj obrazek">
        </div>
        <div class="form-group">
            <label for="Description">Krótki opis nauczyciela:</label><br>
            <textarea class="form-control" id="Description" name="description" rows="3"
                      placeholder="Wpisz krótki opis kategorii"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </div>
</form>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>