<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-24
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>


<form:form action="/admin/create/category" modelAttribute="categoryAdminDto" method="post">
    <div class="container">
        <br>
        <div class="form-group">
            <label for="CategoryName">Nazwa:</label>
            <p class="mb-3 mt-2"><form:errors path="name" class="alert alert-danger mb-2"/></p>
            <form:input path="name" class="form-control" id="CategoryName" placeholder="Wpisz nazwę kategorii"/>
        </div>

        <div class="form-group">
            <label for="Description">Krótki opis kategorii:</label>
            <p class="mb-3 mt-2"><form:errors path="description" class="alert alert-danger mb-2"/></p>
            <form:textarea  path="description" class="form-control" id="Description" rows="3" cols="20" placeholder="Wpisz krótki opis kategorii"/>
        </div>
        <div class="form-group">
            <label for="Image">Załaduj zdjęcie kategorii:</label>
            <p class="mb-3 mt-2"><form:errors path="imagePath" class="alert alert-danger mb-2"/></p>
            <form:input path="imagePath" class="form-control" id="Image" placeholder="Załaduj obrazek " />
        </div>
        <div class="form-group">
            <label for="YouTube">Link do filmu na YouTube:</label>
            <p class="mb-3 mt-2"><form:errors path="youTubeVideoAddress" class="alert alert-danger mb-2"/></p>
            <form:input path="youTubeVideoAddress" class="form-control" id="YouTube" placeholder="Wklej pełny link do filmu na YouTube" />
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Wróć</button>
        </div>
    </div>
</form:form>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>
