<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-27
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eSchool</title>
    <jsp:include page="/model/resources.jsp"/>
    <style>
        td.details-control {
            background: url('https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/47443236_535013490348205_7227095886491287552_n.png?_nc_cat=106&_nc_ht=scontent.fktw2-1.fna&oh=6c914b0613cae87b4a411bd1e1c99b95&oe=5CA73B10') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/47443236_535013490348205_7227095886491287552_n.png?_nc_cat=106&_nc_ht=scontent.fktw2-1.fna&oh=6c914b0613cae87b4a411bd1e1c99b95&oe=5CA73B10') no-repeat center center;
        }
    </style>
</head>


<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="container mt-4">
    <form action="/admin" method="post">
        <div class="table-responsive-sm">

            <table id="table_id" class="display" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Position</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Position</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </form>
</div>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        /* Formatting function for row details - modify as you need */
        function format(d) {
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Full name:</td>' +
                '<td>' + d.name + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extension number:</td>' +
                '<td><img style="width: 250px" alt="super obrazek" src="' + d.imagePath + '" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extra info:</td>' +
                '<td>And any further details here (images etc)...</td>' +
                '</tr>' +
                '</table>';
        }

        $(document).ready(function () {
            var table = $('#table_id').DataTable({
                "ajax": "/admin/rest/show-categories",
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "name"},
                    {"data": "description"}
                ],
                "order": [[1, 'asc']]
            });

            // Add event listener for opening and closing details
            $('#table_id tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        });
    });
</script>

