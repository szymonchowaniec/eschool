<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 2018-11-27
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>
</html>