<%--
  Created by IntelliJ IDEA.
  User: dawidvanrijswijk
  Date: 2018-11-24
  Time: 09:56
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="text-center mt-0 mb-10"
     style="background-image: url(http://hdqwalls.com/wallpapers/boy-playing-guitar-outdoors-5k-0l.jpg); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="mx-auto col-md-6 col-10 eschool-transparent1 p-5">
                <h1>Zarejestruj się</h1>
                <form action="/register" method="post" class="text-left">

                    <div class="form-group"><label for="form1">Imię</label> <input type="text" name="firstName"
                                                                                   class="form-control"
                                                                                   id="form1" placeholder="Adam">
                    </div>
                    <div class="form-group"><label for="form2">Nazwisko</label> <input type="text" name="lastName"
                                                                                       class="form-control"
                                                                                       id="form2"
                                                                                       placeholder="Tyniec">
                    </div>
                    <div class="form-group"><label for="form4">Numer telefonu</label><input type="text" name="phone"
                                                                                            class="form-control"
                                                                                            id="form4"
                                                                                            placeholder="+48 535 841 354">
                    </div>
                    <div class="form-group"><label for="form5">Email</label> <input type="email" name="email"
                                                                                    class="form-control"
                                                                                    id="form5"
                                                                                    placeholder="a.tyniec@gmail.com">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6"><label for="form6">Hasło</label> <input type="password"
                                                                                                 name="password"
                                                                                                 class="form-control"
                                                                                                 id="form6"
                                                                                                 placeholder="••••">
                        </div>
                        <div class="form-group col-md-6"><label for="form7" contenteditable="true">Potwierdź
                            hasło</label> <input type="password" name="confirmPassword" class="form-control" id="form7"
                                                 placeholder="••••">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-check"><input class="form-check-input" type="checkbox" id="form8"
                                                       name="regulations" value="on">
                            <label class="form-check-label" for="form8"> Akceptuje <a href="#">warunki
                                regulaminu</a>
                                oraz wyrażam zgodę na przetwarzanie moich danych osobowych. </label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <select class="form-control mt-2" name="sportCard">
                                <c:forEach var="sportCard" items="${sportCards}" varStatus="loop">
                                    <option>${sportCard.cardName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Zarejestruj się</button>
                </form>
            </div>
        </div>
    </div>
</div>

<jsp:include page="..//model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>

</html>