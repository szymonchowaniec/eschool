<%--
  Created by IntelliJ IDEA.
  User: dawidvanrijswijk
  Date: 2018-11-24
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="container mt-4">
    <form action="/" method="post">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Kategoria</th>
                <th>Nazwa</th>
                <th>Dzień</th>
                <th>Godzina</th>
                <th>Cena</th>
                <th>Liczba wolnych miejsc</th>
                <th>Zdjęcie</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="user" items="${users}" varStatus="loop">
                <tr>
                    <th scope="row">${loop.index + 1}</th>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.lastName}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form>
</div>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>

</html>