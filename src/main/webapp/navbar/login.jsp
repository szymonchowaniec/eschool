<%--
  Created by IntelliJ IDEA.
  User: dawidvanrijswijk
  Date: 2018-11-24
  Time: 09:42
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="py-5 text-center"
     style="background-image: url(https://cdn-images-1.medium.com/max/2000/1*YLlZ96J3p8GFkIh1USVMzg.jpeg); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="mx-auto col-md-6 col-10 eschool-transparent2 p-5">
                <h1 class="mb-4">Zaloguj się </h1>
                <form action="/login" , method="post">
                    <div class="form-group"><input type="email" name="username" class="form-control"
                                                   placeholder="Wpisz email"
                                                   id="form9"></div>
                    <div class="form-group mb-3"><input type="password" name="password" class="form-control"
                                                        placeholder="Wpisz hasło"
                                                        id="form10">
                        <small class="form-text text-muted text-right">
                            <a href="#"> Odzyskaj hasło</a>
                        </small>
                    </div>
                    <button type="submit" class="btn btn-primary">Zakończ</button>
                </form>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../model/footer.jsp"/>

<jsp:include page="../model/script.jsp"/>

</body>

</html>
