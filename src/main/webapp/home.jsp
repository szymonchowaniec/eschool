<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="py-5 text-center mx-3 mb-3">
    <div class="container">
        <div class="row">
            <div style="margin-top: 10px">
                <iframe
                        id="movie-panel"
                        src="https://www.youtube.com/embed/PrqYohBV58o?mute=1&autoplay=1&controls=0&disablekb=0&fs=0&iv_load_policy=3&loop=1&modestbranding=1&rel=0&showinfo=0"
                        frameborder="0" allowfullscreen=""
                        style="position:absolute;pointer-events: none;top:70px;left:0;width:100%;height:100%;">
                </iframe>
            </div>
            <div class="mx-auto p-5 my-5 col-md-8">
                <h1 class="display-4 text-white" style="">Szkolenie rajdowe&nbsp;</h1>
                <p class="lead text-white">Podczas jazdy za kierownicą Subaru poznasz techniki, które sprawią że
                    będziesz czerpał
                    radość z jazdy, przy zachowaniu pełnego bezpieczeństwa. </p>
                <a class="btn btn-outline-secondary text-white" href="subpages/szkolenie-rajdowe.jsp">Więcej
                    informacji</a>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <div class="container-fluid" style="margin-top: 70px">
        <div class="row px-2">
            <div class="col mx-2 pt-5 px-5 mb-3 bg-info" style="">
                <a href="navbar/login.jsp">
                    <h2 class="mt-3"><b>NOVA Szkoła tańca</b></h2>
                    <p class="lead">Oferta kursów tanecznych skierowana jest przede wszystkim do osób dorosłych,
                        ale znajdzie się również coś dla młodzieży.</p>
                    <a class="btn btn-outline-secondary text-white mb-3" onclick="funcClick1()" id="button1">Więcej
                        informacji</a>
                    <img class="img-fluid d-block"
                         src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46517110_519028341898412_6036907079506067456_n.jpg?_nc_cat=105&_nc_ht=scontent.fktw2-1.fna&oh=d3c2872ab3d921631d06711d90b5e478&oe=5CAD3DD2"
                         width="">
                </a>
            </div>
            <div class="row px-2">
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>VIDI Centrum rozwoju kadr</b></h2>
                        <p class="lead">Misja naszej firmy to wspieranie przedsiębiorców i ich personelu, aby osiągali
                            lepsze wyniki w swojej działalności.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" href="#">Więcej informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46660558_1153966421422415_1727670742507585536_n.jpg?_nc_cat=100&_nc_ht=scontent.fktw2-1.fna&oh=1cac0e2c3332d265d46cb0def8842584&oe=5CAC0A95"
                             width="">
                    </a>
                </div>
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>Sushi Master</b></h2>
                        <p class="lead">Warsztaty wprowadzają w tajniki kuchni japońskiej, kursy zaś mogą przygotować
                            uczestników do zawodu Mistrza Sushi.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" onclick="funcClick2()" id="button2">Więcej
                            informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46792894_1166987503458031_2119520782383054848_n.jpg?_nc_cat=105&_nc_ht=scontent.fktw2-1.fna&oh=07936550c947cb99194777aaf3bc4b2e&oe=5C7BE5AE"
                             width="">
                    </a>
                </div>
            </div>
            <div class="row px-2">
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>Just Dive</b></h2>
                        <p class="lead">Należymy do PADI - największej międzynarodowej organizacji szkolącej nurków
                            rekreacyjnych.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" onclick="funcClick3()" id="button3">Więcej
                            informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46984660_445606309302808_8733006871822073856_n.jpg?_nc_cat=102&_nc_ht=scontent.fktw2-1.fna&oh=b879b82046b70fe917af4ddee37d0274&oe=5CAC670D"
                             width="">
                    </a>
                </div>
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>Cloud Team</b></h2>
                        <p class="lead">Celem kursantów jest wdrożenie i utrzymanie zgodności z GDPR w przykładowej
                            organizacji.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" href="#">Więcej informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/46778386_270111453691241_6858667063490117632_n.png?_nc_cat=111&_nc_ht=scontent.fktw2-1.fna&oh=9ceffad3ed230ecdb1a7c68e6a98613b&oe=5C65B882"
                             width="">
                    </a>
                </div>
            </div>
            <div class="row px-2">
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>Żak</b></h2>
                        <p class="lead">Na zajęciach nauczymy Cię projektować i obrabiać grafikę za pomocą
                            komputerowych
                            programów graficznych.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" href="#">Więcej informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/46866215_332880744198401_6822354115729817600_n.jpg?_nc_cat=107&_nc_ht=scontent.fktw2-1.fna&oh=cfcfc58eb49c07c3e4e012231f4b7baf&oe=5CA77D42"
                             width="">
                    </a>
                </div>
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="#">
                        <h2 class="mt-3"><b>BHP Center</b></h2>
                        <p class="lead">Dbając o jakość naszych szkoleń prowadzimy je przy użyciu prezentacji
                            multimedialnych, filmów oraz fantomów szkoleniowych.</p>
                        <a class="btn btn-outline-secondary text-white mb-3" onclick="funcClick4()" id="button4">Więcej
                            informacji</a>
                        <img class="img-fluid d-block"
                             src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46801506_315336705722395_8019055196942368768_n.jpg?_nc_cat=106&_nc_ht=scontent.fktw2-1.fna&oh=33dbf2304ce53ba116f2968fa3a5d08d&oe=5C6F4448"
                             width="">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="/model/footer.jsp"/>

    <jsp:include page="/model/script.jsp"/>

</body>

</html>