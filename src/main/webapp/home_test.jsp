<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>eSchool</title>
</head>

<jsp:include page="/model/resources.jsp"/>

<body>

<jsp:include page="/model/navbar.jsp"/>

<div class="py-5 text-center mx-3 mb-3">
    <div class="container">
        <div class="row">
            <div style="margin-top: 10px">
                <iframe
                        id="movie-panel"
                        src="https://www.youtube.com/embed/PrqYohBV58o?mute=1&autoplay=1&controls=0&disablekb=0&fs=0&iv_load_policy=3&loop=1&modestbranding=1&rel=0&showinfo=0"
                        frameborder="0" allowfullscreen=""
                        style="position:absolute;pointer-events: none;top:70px;left:0;width:100%;height:100%;">
                </iframe>
            </div>
            <div class="mx-auto p-5 my-5 col-md-8">
                <h1 class="display-4 text-white" style="">Szkolenie rajdowe&nbsp;</h1>
                <p class="lead text-white">Podczas jazdy za kierownicą Subaru poznasz techniki, które sprawią że
                    będziesz czerpał
                    radość z jazdy, przy zachowaniu pełnego bezpieczeństwa. </p>
                <a class="btn btn-outline-secondary text-white" href="szkolenie-rajdowe.jsp">Więcej informacji</a>
            </div>
        </div>
    </div>
</div>

<div class="text-center">
    <div class="container-fluid" style="margin-top: 70px">
        <div class="row px-2">
            <c:forEach var="category" items="${categories}">
                <div class="col mx-2 pt-5 px-5 mb-3 bg-info">
                    <a href="login.jsp">
                        <h2 class="mt-3"><b>${category.name}</b></h2>
                        <p class="lead">${category.description}</p>
                        <a class="btn btn-outline-secondary text-white mb-3" onclick="funcClick${category.id}()"
                           id="button${category.id}">Więcej
                            informacji</a>
                        <img class="img-fluid d-block" src=${category.imagePath}>
                    </a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<jsp:include page="/model/footer.jsp"/>

<jsp:include page="/model/script.jsp"/>

</body>

</html>