<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-expand-md bg-dark navbar-dark mb-0">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right mx-auto" type="button" data-toggle="collapse"
                data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false"
                aria-label="Toggle navigation" style=""><span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse text-center justify-content-between" id="navbar2SupportedContent">
            <a class="p-2 text-secondary" href="/">
                <img src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/47443236_535013490348205_7227095886491287552_n.png?_nc_cat=106&_nc_ht=scontent.fktw2-1.fna&oh=6c914b0613cae87b4a411bd1e1c99b95&oe=5CA73B10"
                     alt="logo" width="25" height="25">
            </a>
            <a class="p-2 text-white" href="#">Plan tygodnia</a>
            <a class="p-2 text-white" href="/courses">Wybierz kurs</a>


            <sec:authorize access="not hasAnyRole('ROLE_ADMIN','ROLE_SUPER_ADMIN','ROLE_USER')">
                <a class="p-2 text-white" href="/register">Zarejestruj się</a>
                <a class="p-2 text-white" href="/login">Zaloguj się</a>
            </sec:authorize>

            <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_SUPER_ADMIN')">
                <a class="p-2 text-white" href="/admin">Panel admina: <sec:authentication
                        property="principal.firstName"/></a>
            </sec:authorize>

            <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER')">
                <a class="p-2 text-white" href="/"> Wyloguj się</a>
            </sec:authorize>

            <sec:authorize access="hasAnyRole('ROLE_USER')">
                <a class="p-2 text-white" href="#">Witaj, <sec:authentication
                        property="principal.firstName"/>!</a>
            </sec:authorize>
        </div>
    </div>
</nav>
