<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<footer>
    <div class="py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center d-md-flex align-items-center"><a>
                    <img src="https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/47297821_1945272589112333_873693736389836800_n.png?_nc_cat=105&_nc_ht=scontent.fktw2-1.fna&oh=5e60c1780166693150736c9bb40ab977&oe=5CA1063F"
                         alt="logo" width="95" height="50">
                </a>
                    <ul class="nav mx-md-auto d-flex justify-content-center">

                        <li class="nav-item"><a class="nav-link active" href="home_test.jsp">Strona główna</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Aktualności</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Kontakt</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">O nas</a></li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12 d-flex align-items-center justify-content-md-between justify-content-center my-2">
                            <a href="#">
                                <i class="d-block fa fa-facebook-official text-muted fa-lg mx-2"></i>
                            </a> <a href="#">
                            <i class="d-block fa fa-instagram text-muted fa-lg mx-2"></i>
                        </a> <a href="#">
                            <i class="d-block fa fa-twitter text-muted fa-lg ml-2"></i>
                        </a></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="mt-2 mb-0">© 2018 Dawid&Jacek. Wszystkie prawa zastrzeżone</p>
                </div>
            </div>
        </div>
    </div>
</footer>