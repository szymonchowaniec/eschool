package pl.sda.eschool.eschool.exceptions;

public class UsernameNotFountException extends Exception {

    public UsernameNotFountException(String errorMessage) {
        super(errorMessage);
    }
}
