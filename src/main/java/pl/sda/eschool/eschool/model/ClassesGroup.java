package pl.sda.eschool.eschool.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class ClassesGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "classes_group_id")
    private Long id;

    private String name;

    @Column (nullable = false)
    private String day;

    @Column (nullable = false)
    private LocalTime hour;

    @Column (name = "already_payed")
    private BigDecimal alreadyPayed;

    @Column (name = "rate_percentage")
    private Double ratePercentage;

    private boolean continues;

    @Column (name = "reg_is_open")
    private boolean registrationIsOpen;

    @Column (name = "is_active")
    private boolean isActive;

    private Integer capacity;

    @ManyToOne
    @JoinColumn (name = "course_id")
    private Course course;

    @Column (name = "my_partner")
    private User myPartner;

    @Column (name = "taken_places")
    private Integer takenPlaces;


    @ManyToOne
    @JoinColumn (name = "tutor_id")
    private Tutor tutor;



}
