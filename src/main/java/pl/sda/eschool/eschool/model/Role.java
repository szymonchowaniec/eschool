package pl.sda.eschool.eschool.model;

public enum Role {

    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_GUEST
}
