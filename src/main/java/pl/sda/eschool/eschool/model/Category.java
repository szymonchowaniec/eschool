package pl.sda.eschool.eschool.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_category")
    private Long id;

    @Column (nullable = false)
    private String name;

    @Column (nullable = false, length = 500)
    private String imagePath;

    @Column (name = "youtube_id")
    private String youTubeVideoAddress;

    @Column (nullable = false, length = 500)
    private String description;

    @Column (name = "is_active")
    private boolean isActive;

    @OneToMany (mappedBy = "category")
    private List<Course> courses;


}
