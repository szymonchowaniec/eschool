package pl.sda.eschool.eschool.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Privilege implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<UserRole> userRoles;


}
