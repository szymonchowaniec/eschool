package pl.sda.eschool.eschool.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tutor {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "first_name", nullable = false)
    private String firstName;

    @Column (name = "last_name", nullable = false)
    private String lastName;

    @Column (name = "image", nullable = false)
    private String imagePath;

    @Column(nullable = false)
    private String description;

    @OneToMany (mappedBy = "tutor")
    private List<ClassesGroup> courses;

}
