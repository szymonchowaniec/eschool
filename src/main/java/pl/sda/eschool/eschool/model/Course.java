package pl.sda.eschool.eschool.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;

@Entity
@Getter
@Setter
public class Course implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_course")
    private Long id;

    @Column (nullable = false)
    private String name;

    @Column (nullable = false, length = 500)
    private String description;

    @ManyToOne
    @JoinColumn (name="course_category_id")
    private Category category;

    private String imagePath;

    @Column (name = "course_youtube_video")
    private String youTubeVideoAddress;

    @Column (nullable = false)
    private BigDecimal price;

    @Column (name = "is_active")
    private boolean isActive;

    @OneToMany (mappedBy = "course")
    private List<ClassesGroup> groups;




}
