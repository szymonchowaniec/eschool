package pl.sda.eschool.eschool.model;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class CategoriesTableView {

    List<Category> data;

}
