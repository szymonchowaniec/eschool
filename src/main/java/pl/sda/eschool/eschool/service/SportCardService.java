package pl.sda.eschool.eschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.eschool.eschool.model.SportCard;
import pl.sda.eschool.eschool.repository.SportCardRepository;
import java.util.List;

@Service
public class SportCardService {

    private final SportCardRepository sportCardRepository;

    @Autowired
    public SportCardService(SportCardRepository sportCardRepository) {
        this.sportCardRepository = sportCardRepository;
    }

    public List<SportCard> findAllSportCards() {
        return sportCardRepository.findAll();
    }


}
