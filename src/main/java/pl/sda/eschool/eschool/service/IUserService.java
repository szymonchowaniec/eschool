package pl.sda.eschool.eschool.service;

import pl.sda.eschool.eschool.dto.UserRegistrationDTO;

public interface IUserService {

    void create (UserRegistrationDTO dto);

}
