package pl.sda.eschool.eschool.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface IUserDetailsService {

    UserDetails loadUserByUserName(String userName);


}
