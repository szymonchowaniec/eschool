package pl.sda.eschool.eschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.eschool.eschool.assemblers.UserAssembler;
import pl.sda.eschool.eschool.dto.UserRegistrationDTO;
import pl.sda.eschool.eschool.model.User;
import pl.sda.eschool.eschool.model.UserRole;
import pl.sda.eschool.eschool.repository.UserRepository;
import pl.sda.eschool.eschool.repository.UserRoleRepository;


@Service
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserAssembler userAssembler;
    private final PasswordEncoder passwordEncoder;

    private static final String DEFAULT_ROLE = "ROLE_USER";

    @Autowired
    public UserService(UserRepository userRepository, UserRoleRepository userRoleRepository, UserAssembler userAssembler, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.userAssembler = userAssembler;
        this.passwordEncoder = passwordEncoder;
    }

    public void create(UserRegistrationDTO dto) {
        UserRole defaultRole = userRoleRepository.findByName(DEFAULT_ROLE);
        User user = userAssembler.fromUserRegistrationDto(dto);
        user.getUserRoles().add(defaultRole);
        String passwordHash = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordHash);
        userRepository.save(user);
    }


}
