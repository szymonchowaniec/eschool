package pl.sda.eschool.eschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.eschool.eschool.assemblers.CategoryAssembler;
import pl.sda.eschool.eschool.dto.CategoryAdminDTO;
import pl.sda.eschool.eschool.dto.CategoryAdminManageDTO;
import pl.sda.eschool.eschool.dto.CategoryGuestDTO;
import pl.sda.eschool.eschool.model.CategoriesTableView;
import pl.sda.eschool.eschool.model.Category;
import pl.sda.eschool.eschool.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService implements ICategoryService {

    private final CategoryRepository repository;
    private final CategoryAssembler assembler;

    @Autowired
    public CategoryService(CategoryRepository repository, CategoryAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @Override
    public void create(CategoryAdminDTO dto) {
        Category category = new Category();
        category.setName(dto.getName());
        category.setDescription(dto.getDescription());
        category.setImagePath(dto.getImagePath());
        category.setYouTubeVideoAddress(dto.getYouTubeVideoAddress());
        category.setActive(false);
        repository.save(category);
    }

    public List<CategoryGuestDTO> showActiveCategories() {
        return repository.findAll()
                .stream()
                .filter(x -> x.isActive())
                .map(assembler::toGuestCategoryDTO)
                .collect(Collectors.toList());

    }

    public List<CategoryAdminManageDTO> showCategoriesToManage(){
        return repository.findAll()
                .stream()
                .map(assembler::toCategoryAdminManageDTO)
                .collect(Collectors.toList());
    }


    public List<Category> findAll(){
        return repository.findAll();
    }


    private String prepareYouTubeVideoID(String youtube){
        int startIndex = youtube.indexOf("=");
        int endIndex = youtube.length();
        String videoID = youtube.substring(startIndex+1, endIndex);
        return videoID;
    }

    public CategoriesTableView prepareCategoriesTableView(){
        CategoriesTableView view = new CategoriesTableView();
        List<Category> data = new ArrayList<>();
        data = repository.findAll();
        view.setData(data);
        return view;
    }

}
