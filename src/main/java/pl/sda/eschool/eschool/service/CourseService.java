package pl.sda.eschool.eschool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.eschool.eschool.dto.CourseAdminDTO;
import pl.sda.eschool.eschool.model.Course;
import pl.sda.eschool.eschool.repository.CourseRepository;

@Service
public class CourseService implements ICourseService{

    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    @Override
    public void create(CourseAdminDTO dto) {
        Course course = new Course();
        course.setName(dto.getName());
        course.setDescription(dto.getDescription());
        course.setImagePath(dto.getImagePath());
        course.setYouTubeVideoAddress(dto.getYouTubeVideoAddress());
        course.setPrice(dto.getPrice());
        courseRepository.save(course);
    }



}
