package pl.sda.eschool.eschool.service;

import pl.sda.eschool.eschool.dto.CourseAdminDTO;

public interface ICourseService {

    void create(CourseAdminDTO dto);

}
