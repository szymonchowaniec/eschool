package pl.sda.eschool.eschool.service;

import pl.sda.eschool.eschool.dto.CategoryAdminDTO;
import pl.sda.eschool.eschool.dto.CategoryGuestDTO;

import java.util.List;

public interface ICategoryService {

    void create (CategoryAdminDTO dto);

    List<CategoryGuestDTO> showActiveCategories();
}
