package pl.sda.eschool.eschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.eschool.eschool.dto.UserRegistrationDTO;
import pl.sda.eschool.eschool.service.SportCardService;
import pl.sda.eschool.eschool.service.UserService;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class UserRegistrationController {

    private final UserService userService;
    private final SportCardService sportCardService;


    @Autowired
    public UserRegistrationController(UserService userService, SportCardService sportCardService) {
        this.userService = userService;
        this.sportCardService = sportCardService;
    }


    @GetMapping("/register")
    public String showInvalidRegistrationPage(Model model) {
        model.addAttribute("sportCards", sportCardService.findAllSportCards());
        return "/navbar/register";
    }

    @GetMapping("/login")
    public String returnLoginView() {
        return "/navbar/login";
    }


    @PostMapping("/register")
    public String createNewUser(
            @ModelAttribute @Valid UserRegistrationDTO form, BindingResult result, Model model) {

        if (result.hasErrors()) {
            model.addAttribute("errors", new ArrayList<>());
            return "/navbar/register";
        }
        userService.create(form);
        return "redirect:/";
    }

}
