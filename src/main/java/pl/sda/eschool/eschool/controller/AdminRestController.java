package pl.sda.eschool.eschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.eschool.eschool.model.CategoriesTableView;
import pl.sda.eschool.eschool.model.Category;
import pl.sda.eschool.eschool.model.ClassesGroup;
import pl.sda.eschool.eschool.model.Course;
import pl.sda.eschool.eschool.repository.CategoryRepository;
import pl.sda.eschool.eschool.repository.ClassessGroupRepository;
import pl.sda.eschool.eschool.repository.CourseRepository;
import pl.sda.eschool.eschool.service.CategoryService;

import java.util.List;

@RestController
public class AdminRestController {

    private final CategoryRepository categoryRepository;
    private final CourseRepository courseRepository;
    private final ClassessGroupRepository classessGroupRepository;
    private final CategoryService categoryService;

    @Autowired
    public AdminRestController(CategoryRepository categoryRepository, CourseRepository courseRepository, ClassessGroupRepository classessGroupRepository, CategoryService categoryService) {
        this.categoryRepository = categoryRepository;
        this.courseRepository = courseRepository;
        this.classessGroupRepository = classessGroupRepository;
        this.categoryService = categoryService;
    }


    @GetMapping ("/admin/rest/show-categories")
    @ResponseBody
    public CategoriesTableView getListOfCategoriesToJSTable(){
        return categoryService.prepareCategoriesTableView();
    }

    @GetMapping ("/admin/rest/show-courses")
    @ResponseBody
    public List<Course> getListOfCoursesToJSTable(){
        return courseRepository.findAll();
    }

    @GetMapping ("/admin/rest/show-groups")
    @ResponseBody
    public List<ClassesGroup> getListOfClassessGroupsToJSTable(){
        return classessGroupRepository.findAll();
    }
}
