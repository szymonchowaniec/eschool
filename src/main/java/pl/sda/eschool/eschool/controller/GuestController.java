package pl.sda.eschool.eschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.sda.eschool.eschool.service.CategoryService;
import pl.sda.eschool.eschool.service.SportCardService;

@Controller
public class GuestController {

    private final SportCardService sportCardService;
    private final CategoryService categoryService;

    @Autowired
    public GuestController(SportCardService sportCardService, CategoryService categoryService) {
        this.sportCardService = sportCardService;
        this.categoryService = categoryService;
    }


    @GetMapping ("/")
    public String showRegistrationForm(Model model) {
        model.addAttribute("categories", categoryService.showActiveCategories());
        return "home_test";
    }

    @GetMapping("/courses")
    public String showCourseInMainPage (){
        return "/navbar/courses";
    }


}
