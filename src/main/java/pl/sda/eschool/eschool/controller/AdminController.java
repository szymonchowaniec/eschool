package pl.sda.eschool.eschool.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.eschool.eschool.dto.CategoryAdminDTO;
import pl.sda.eschool.eschool.dto.CourseAdminDTO;
import pl.sda.eschool.eschool.service.CategoryService;
import pl.sda.eschool.eschool.service.CourseService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;

@Controller
public class AdminController {

    private final CategoryService categoryService;
    private final CourseService courseService;

    @Autowired
    public AdminController(CategoryService categoryService, CourseService courseService) {
        this.categoryService = categoryService;
        this.courseService = courseService;
    }

    @ModelAttribute("courseAdminDto")
    public CourseAdminDTO getCourseAdminObject() {
        return new CourseAdminDTO();
    }

    @ModelAttribute("categoryAdminDto")
    public CategoryAdminDTO getCategoryAdminDTO() {
        return new CategoryAdminDTO();
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_ADMIN')")
    @GetMapping("/admin")
    public String showAdminPanel(Principal principal) {
        return "/admin/admin-panel";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/create/course")
    public String showCreateCourse(Principal principal, Model model) {
        model.addAttribute("categories", categoryService.findAll());
        return "/admin/create-new-course";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @PostMapping("/admin/create/course")
    public String createNewCourse(@ModelAttribute("courseAdminDto") @Valid CourseAdminDTO dto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("categories", categoryService.findAll());
            return "/admin/create-new-course";
        }
        courseService.create(dto);
        return "redirect:/admin/create/course/success";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/create/course/success")
    public String courseCreatedSuccessfull(Principal principal) {
        return "/success-pages/success-course";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/create/category")
    public String showCreateCategories(Principal principal) {
        return "/admin/create-new-category";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @PostMapping("/admin/create/category")
    public String createNewCategory(@ModelAttribute("categoryAdminDto") @Valid CategoryAdminDTO dto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("errors", new ArrayList<>());
            return "/admin/create-new-category";
        }
        categoryService.create(dto);
        return "redirect:/admin/create/category/success";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/create/category/success")
    public String categoryCreatedSuccessfull(Principal principal) {
        return "/success-pages/success-category";
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/manage/category")
    public String manageCategories(Principal principal) {
        return "/admin/manage-categories";
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @GetMapping("/admin/manage/course")
    public String manageCourses(Principal principal) {
        return "/admin/manage-courses";
    }


}
