package pl.sda.eschool.eschool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@ComponentScan
@SpringBootApplication
public class EschoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(EschoolApplication.class, args);
    }

    @Bean
    PasswordEncoder getEncoder(){
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
