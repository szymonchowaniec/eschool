package pl.sda.eschool.eschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import pl.sda.eschool.eschool.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    UserDetails findByEmail(String userName);

}
