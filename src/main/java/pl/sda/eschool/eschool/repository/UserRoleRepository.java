package pl.sda.eschool.eschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.eschool.eschool.model.UserRole;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole,Long> {

    UserRole findByName(String role);

}
