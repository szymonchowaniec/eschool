package pl.sda.eschool.eschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.eschool.eschool.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
