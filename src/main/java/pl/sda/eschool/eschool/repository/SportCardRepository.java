package pl.sda.eschool.eschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.eschool.eschool.model.SportCard;

import java.util.List;

@Repository
public interface SportCardRepository extends JpaRepository<SportCard, Integer> {

}
