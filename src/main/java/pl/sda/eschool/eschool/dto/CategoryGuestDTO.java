package pl.sda.eschool.eschool.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryGuestDTO {

    private Long id;
    private String name;
    private String imagePath;
    private String youTubeVideoID;
    private String description;
}
