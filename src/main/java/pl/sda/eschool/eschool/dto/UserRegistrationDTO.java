package pl.sda.eschool.eschool.dto;

import lombok.Getter;
import lombok.Setter;
import pl.sda.eschool.eschool.annotations.FieldMatch;
import javax.validation.constraints.Email;

@Getter
@Setter
@FieldMatch(first = "password", second = "passwordConfirm", message = "Hasła muszą być spójne")
public class UserRegistrationDTO {

    @Email
    private String email;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
    private String phone;


}
