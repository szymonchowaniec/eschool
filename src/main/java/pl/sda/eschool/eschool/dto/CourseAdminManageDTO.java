package pl.sda.eschool.eschool.dto;

import lombok.Getter;
import lombok.Setter;
import pl.sda.eschool.eschool.model.Category;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class CourseAdminManageDTO {

    private Long id;

    @Size(min=10, max=100)
    private String name;

    @Size(min=25, max=255)
    private String description;

    private Category category;

    @Min(1)
    @Digits(integer = 6, fraction = 2)
    private BigDecimal price;

    @Size(min=1)
    private String imagePath;

    @Size(min=1)
    private String youTubeVideoAddress;

    private boolean isActive;


}
