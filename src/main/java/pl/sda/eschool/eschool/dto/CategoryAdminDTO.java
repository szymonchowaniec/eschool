package pl.sda.eschool.eschool.dto;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CategoryAdminDTO {

    @Size(min=10, max=100)
    private String name;

    @Size(min=10, max=500)
    private String description;

    @Size (min = 1)
    private String imagePath;

    @Size (min = 1)
    private String youTubeVideoAddress;

}
