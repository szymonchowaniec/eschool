package pl.sda.eschool.eschool.assemblers;

import pl.sda.eschool.eschool.dto.UserRegistrationDTO;
import pl.sda.eschool.eschool.model.User;

public interface IUserAssembler {

    UserRegistrationDTO toUserRegistrationDto(User user);

    User fromUserRegistrationDto(UserRegistrationDTO userRegistrationDTO);

}
