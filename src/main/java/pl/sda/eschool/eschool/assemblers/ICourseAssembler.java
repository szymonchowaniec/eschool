package pl.sda.eschool.eschool.assemblers;

import pl.sda.eschool.eschool.dto.CourseAdminManageDTO;
import pl.sda.eschool.eschool.model.Course;

public interface ICourseAssembler {

    CourseAdminManageDTO toCourseAdminManageDTO (Course course);


}
