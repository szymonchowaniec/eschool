package pl.sda.eschool.eschool.assemblers;

import org.springframework.stereotype.Component;
import pl.sda.eschool.eschool.dto.UserRegistrationDTO;
import pl.sda.eschool.eschool.model.User;

@Component
public class UserAssembler implements IUserAssembler {

    @Override
    public UserRegistrationDTO toUserRegistrationDto(User user) {
        final UserRegistrationDTO dto = new UserRegistrationDTO();
        dto.setPassword(user.getPassword());
        dto.setEmail(user.getEmail());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setPhone(user.getPhone());
        return dto;
    }

    @Override
    public User fromUserRegistrationDto(UserRegistrationDTO dto) {
        User user = new User();
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setPhone(dto.getPhone());
        return user;
    }
}
