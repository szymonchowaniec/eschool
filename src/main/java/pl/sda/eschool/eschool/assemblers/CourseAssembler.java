package pl.sda.eschool.eschool.assemblers;

import pl.sda.eschool.eschool.dto.CourseAdminManageDTO;
import pl.sda.eschool.eschool.model.Course;

public class CourseAssembler implements ICourseAssembler{

    @Override
    public CourseAdminManageDTO toCourseAdminManageDTO(Course course) {
        CourseAdminManageDTO dto = new CourseAdminManageDTO();
        dto.setId(course.getId());
        dto.setName(course.getName());
        dto.setDescription(course.getDescription());
        dto.setCategory(course.getCategory());
        dto.setPrice(course.getPrice());
        dto.setImagePath(course.getImagePath());
        dto.setYouTubeVideoAddress(course.getYouTubeVideoAddress());
        dto.setActive(course.isActive());
        return dto;
    }
}
