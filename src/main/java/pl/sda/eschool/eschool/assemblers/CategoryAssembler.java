package pl.sda.eschool.eschool.assemblers;

import org.springframework.stereotype.Component;
import pl.sda.eschool.eschool.dto.CategoryAdminManageDTO;
import pl.sda.eschool.eschool.dto.CategoryGuestDTO;
import pl.sda.eschool.eschool.model.Category;

@Component
public class CategoryAssembler implements ICategoryAssembler {


    @Override
    public CategoryGuestDTO toGuestCategoryDTO(Category category) {
        final CategoryGuestDTO dto = new CategoryGuestDTO();
        dto.setId(category.getId());
        dto.setName(category.getName());
        dto.setDescription(category.getDescription());
        dto.setImagePath(category.getImagePath());
        dto.setYouTubeVideoID(category.getYouTubeVideoAddress());
        return dto;
    }

    @Override
    public Category fromGuestCategoryDTO(CategoryGuestDTO dto) {
        Category category = new Category();
        category.setName(dto.getName());
        category.setDescription(dto.getDescription());
        category.setImagePath(dto.getImagePath());
        category.setYouTubeVideoAddress(dto.getYouTubeVideoID());
        return category;
    }

    @Override
    public CategoryAdminManageDTO toCategoryAdminManageDTO(Category category) {
        final CategoryAdminManageDTO dto = new CategoryAdminManageDTO();
        dto.setId(category.getId());
        dto.setName(category.getName());
        dto.setDescription(category.getDescription());
        dto.setImagePath(category.getImagePath());
        dto.setYouTubeVideoAddress(category.getYouTubeVideoAddress());
        dto.setActive(category.isActive());
        return dto;
    }



}
