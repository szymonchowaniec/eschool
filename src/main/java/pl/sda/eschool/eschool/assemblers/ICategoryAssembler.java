package pl.sda.eschool.eschool.assemblers;

import pl.sda.eschool.eschool.dto.CategoryAdminManageDTO;
import pl.sda.eschool.eschool.dto.CategoryGuestDTO;
import pl.sda.eschool.eschool.model.Category;

public interface ICategoryAssembler {

    CategoryGuestDTO toGuestCategoryDTO(Category category);

    Category fromGuestCategoryDTO(CategoryGuestDTO dto);

    CategoryAdminManageDTO toCategoryAdminManageDTO (Category category);


}
