package pl.sda.eschool.eschool.alerts;

import org.springframework.beans.factory.annotation.Value;

public class Alerts {

    @Value("${badValueForCourseName}")
    public static String courseBadDName;

    @Value("${badValueForCourseDescription}")
    public static String courseBadDescription;

}
