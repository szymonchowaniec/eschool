ALTER DATABASE jackos91_eschool CHARACTER SET utf8 COLLATE utf8_polish_ci;

INSERT INTO user (id_user, email, password, first_name, last_name, phone, regulations)
VALUES (1, 'superadmin@eschool.pl', '{noop}superadmin', 'superadmin', 'eschool', '123456789', false);

INSERT INTO user (id_user, email, password, first_name, last_name, phone, regulations)
VALUES (2, 'admin@eschool.pl', '{noop}admin', 'admin', 'eschool', '111222333', false);

INSERT INTO user (id_user, email, password, first_name, last_name, phone, regulations)
VALUES (3, 'user1@eschool.pl', '{noop}user1', 'jacek', 'mucha', '999999999', false);


INSERT INTO user_roles (id, name) value (1,'ROLE_SUPER_ADMIN');
INSERT INTO user_roles (id, name) value (2,'ROLE_ADMIN');
INSERT INTO user_roles (id, name) value (3,'ROLE_USER');

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'NOVA Szkoła tańca'
,'Oferta kursów tanecznych skierowana jest przede wszystkim do osób dorosłych, ale znajdzie się również coś dla młodzieży.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46517110_519028341898412_6036907079506067456_n.jpg?_nc_cat=105&_nc_ht=scontent.fktw2-1.fna&oh=d3c2872ab3d921631d06711d90b5e478&oe=5CAD3DD2'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'VIDI Centrum rozwoju kadr'
,'Misja naszej firmy to wspieranie przedsiębiorców i ich personelu, aby osiągali lepsze wyniki w swojej działalności.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46660558_1153966421422415_1727670742507585536_n.jpg?_nc_cat=100&_nc_ht=scontent.fktw2-1.fna&oh=1cac0e2c3332d265d46cb0def8842584&oe=5CAC0A95'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'Sushi Master'
,'Warsztaty wprowadzają w tajniki kuchni japońskiej, kursy zaś mogą przygotować uczestników do zawodu Mistrza Sushi.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46792894_1166987503458031_2119520782383054848_n.jpg?_nc_cat=105&_nc_ht=scontent.fktw2-1.fna&oh=07936550c947cb99194777aaf3bc4b2e&oe=5C7BE5AE'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'Just Dive'
,'Należymy do PADI - największej międzynarodowej organizacji szkolącej nurków rekreacyjnych.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46984660_445606309302808_8733006871822073856_n.jpg?_nc_cat=102&_nc_ht=scontent.fktw2-1.fna&oh=b879b82046b70fe917af4ddee37d0274&oe=5CAC670D'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'Cloud Team'
,'Celem kursantów jest wdrożenie i utrzymanie zgodności z GDPR w przykładowej organizacji.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/46778386_270111453691241_6858667063490117632_n.png?_nc_cat=111&_nc_ht=scontent.fktw2-1.fna&oh=9ceffad3ed230ecdb1a7c68e6a98613b&oe=5C65B882'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'Żak'
,'Na zajęciach nauczymy Cię projektować i obrabiać grafikę za pomocą komputerowych programów graficznych.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/46866215_332880744198401_6822354115729817600_n.jpg?_nc_cat=107&_nc_ht=scontent.fktw2-1.fna&oh=cfcfc58eb49c07c3e4e012231f4b7baf&oe=5CA77D42'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO category (name, description, image_path, youtube_id, is_active)
value
(
'BHP Center'
,'Dbając o jakość naszych szkoleń prowadzimy je przy użyciu prezentacji multimedialnych, filmów oraz fantomów szkoleniowych.'
,'https://scontent.fktw2-1.fna.fbcdn.net/v/t1.15752-9/s2048x2048/46801506_315336705722395_8019055196942368768_n.jpg?_nc_cat=106&_nc_ht=scontent.fktw2-1.fna&oh=33dbf2304ce53ba116f2968fa3a5d08d&oe=5C6F4448'
,'Qyl_7tf4UiU'
,true
);

INSERT INTO day_name (day_name) value ('poniedziałek');
INSERT INTO day_name (day_name) value ('wtorek');
INSERT INTO day_name (day_name) value ('środa');
INSERT INTO day_name (day_name) value ('czwartek');
INSERT INTO day_name (day_name) value ('piątek');
INSERT INTO day_name (day_name) value ('sobota');
INSERT INTO day_name (day_name) value ('niedziela');


INSERT INTO users_roles (user_id, role_id) values (1, 1);
INSERT INTO users_roles (user_id, role_id) values (2, 2);
INSERT INTO users_roles (user_id, role_id) values (3, 3);


INSERT INTO sport_card (card_name) VALUES ('Nie mam żadnej karty');
INSERT INTO sport_card (card_name) VALUES ('MultiSport Plus');
INSERT INTO sport_card (card_name) VALUES ('FitProfit');
INSERT INTO sport_card (card_name) VALUES ('FitFlex');
INSERT INTO sport_card (card_name) VALUES ('My Benefit');
INSERT INTO sport_card (card_name) VALUES ('OK System');
INSERT INTO sport_card (card_name) VALUES ('PodajDalej');
INSERT INTO sport_card (card_name) VALUES ('SODEXO');
INSERT INTO sport_card (card_name) VALUES ('UP BONUS');



